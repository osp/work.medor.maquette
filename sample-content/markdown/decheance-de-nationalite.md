Déchéance de&nbsp;nationalité
=============================

Est-il nécessaire de rappeler ce 7 janvier et ce Charlie que nous fûmes (ou pas) ? Dans la semaine qui suit, la cité de la laine vire cité de la haine. La police fédérale abat à Verviers deux islamistes revenant de Syrie, munis “d’armes de guerre et d’armes de poing”. D’armes, il en sera question dans la rue, avec les treillis de l’armée qui protègent les lieux publics stratégiques jusqu’à nouvel ordre (militaire ?). 

C’est dans cette atmosphère belliqueuse que le gouvernement Michel annonce ses 12 mesures pour lutter contre le radicalisme. Parmi celles-ci, le renforcement de la déchéance de la nationalité. 

La décision
-----------

Dès octobre 2014, l’accord de gouvernement Michel parlait «d’adapter les conditions permettant la déchéance de la nationalité. » Le sujet est donc sur la table. La cible désignée : les jeunes d’origine incertaine partis en Syrie. Et qui sont invités à y rester…
Si c’est le gouvernement Di Rupo qui a étendu fin décembre 2012 la déchéance de nationalité aux faits de terrorisme, celui de Michel durcir le ton, bomber le torse, confisquer la Kalachnikov. 

La mesure ? Aujourd’hui, peuvent être déchues les personnes qui ont obtenu la nationalité après leur majorité, qui sont belges (avec double nationalité) depuis moins de dix ans et qui sont condamnées à une peine de plus de cinq ans (sans sursis) pour des infractions spécifiques (actes terroristes mais aussi génocide, crime de guerre, etc.). La loi à venir ferait en sorte que la limite de dix ans saute. Ce principe a été approuvé en commission de la Justice de la Chambre le 1<sup>er</sup> juillet. Et le Gouvernement demande l'urgence pour le vote de cette loi. Ca sent à plein nez les travaux à expédier avant les vacances…

Efficace ?
----------

Les deux personnes abattues à Verviers étaient belges de naissance. Le chef présumé de la cellule démantelée, Abdelhamid Abaaoud, est également belge. 

Géraldine Henneghien, dont le fils est parti en Syrie, a créé le collectif Les Parents Concernés, rassemblant les proches de jeunes partis combattre au côté de Daech. Elle est formelle, ils sont tous belges de naissance : « *Sur les 25 cas recensés par cette association, aucun ne sera concerné par cette modification de loi.* » 

L’avis de l’expert
------------------

Patrick Wautelet (ULg), enseigne le droit de la nationalité : « *Cette mesure poursuit deux objectifs. L’un psychologique qui consiste à rassurer la population, envoyant le signal qu’on s’occupe de la sécurité. Le second, sous-jacent, serait d’empêcher le retour des personnes parties combattre en Syrie ou Irak, via une condamnation par défaut. (…) On est clairement dans de la politique réactive à des flammèches d’actualité. Depuis 2012, la Belgique s’est dotée d’un cadre pour lutter contre et prévenir le terrorisme. Imagine-t-on vraiment que cette nouvelle donne sera le remède ultime pour éviter que des jeunes aillent en Syrie ou en Irak ? Dans ce cas précis, on s’inscrit dans la réaction symbolique.* »
