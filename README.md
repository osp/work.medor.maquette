Dans ce dépôt, la maquette de Médor en HTML/CSS.


Pour démarrer
=============

À l'aide de votre terminal, déplacer vous à la racine de ce dépôt. Certaines
dépendences ne sont pas inclusent. Nous utilisons Bower pour ler gérer.
Assurez-vous tout d'abord d'avoir bower installé.

    sudo npm -global install bower

Puis, installez les dépenance en tapant:

    bower install

Ensuite, lancez le mini-serveur Python, requis pour pouvoir utiliser Less CSS:

    python2 -m SimpleHTTPServer

Puis rendez-vous dans votre navigateur et visitez l'adresse
`http://localhost:8000`.


Comment générer le fichier HTML contenant les articles?
=======================================================

Il vous faut tout d'abord installer les dépendances nécessaires, le plus simple
est d'utiliser le gestionnaire de paquets de python:

    virtualenv --no-site-packages venv
    source venv/bin/activate
    pip install -r requirements.txt

Ensuite il suffit de taper:

    make

Cette commande compile les fichiers markdown du dossier
`sample-content/markdown` vers le dossier `sample-content/html`.
