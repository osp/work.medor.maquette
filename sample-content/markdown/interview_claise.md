Interview - Michel Claise

« Deux contrats ont été mis sur ma tête »
=============================

Parabole acide sur le dur métier de juge dans un pays de l’Est miné par la corruption, le dernier roman de Michel Claise interpelle : quels sont les rapports entre ce réel fictionnalisé et la réalité quotidienne d’un juge d’instruction en Belgique ? Interview vérité.
{: .chapeau}

Coudre ses poches, c’est refuser de succomber à la corruption. Et dans certains pays, une telle ambition morale peut coûter extrêmement cher. Le dernier roman cosigné par le juge d’instruction bruxellois Michel Claise, Les poches cousues, relate l’histoire vraie d’un juge traqué par la mafia dans un pays de l’ex-bloc communiste – probablement la Bulgarie. Le magistrat sera contraint de fuir sa terre natale, gangrénée par la corruption, après que sa fille aînée a été abattue en pleine rue. Un meurtre commandité par un puissant maffieux russe…
Exfiltré en Belgique en 2002, Alain-Charles Faidherbe – c’est son nouveau nom – a dû tout reconstruire. Il est aujourd’hui, à 50 ans, juriste au parquet de Bruxelles après avoir étudié le droit belge à l’université de Liège. Son ami Michel Claise l’a épaulé pour coucher sur papier une histoire qui ne s’invente pas.
L’occasion de dresser des parallèles entre les expériences singulières des cosignataires. Qui ont tous deux subi, à des degrés divers, des tentatives de corruption, des manœuvres de déstabilisation et des menaces de mort. L’occasion, aussi, de s’interroger sur la justice et le métier de juge d’instruction en Belgique.

**Comment est né ce roman ?**
Michel Claise : Quand Alain-Charles m’a raconté son histoire, j’ai vraiment été transpercé. Pour moi, il fallait consigner tout ça pour en conserver la mémoire. Il m’a alors tendu un épais manuscrit mêlant récit et dialogues rédigé dans un français incertain peu après son arrivée en Belgique. J’ai tout réécrit en inventant plusieurs scènes, et lui ai envoyé le texte chapitre par chapitre. A l’exception d’une erreur de date, il n’a rien corrigé. Par contre j’ai expurgé du récit deux faits que je ne voulais pas raconter. Deux faits aussi terribles que la mort de sa fille. Cosigner ce livre était pour lui, je pense, une façon de combattre la culpabilité qui le ronge comme un acide.

**Pourquoi avoir choisi la fiction plutôt que le témoignage direct ?**
Parce qu’on n’a pas voulu en faire un livre politique, même si finalement ça l’est un peu quand même. Emporteriez-vous en vacances un essai sur la vie de cet homme ? Les gens s’imaginent acheter un roman qui va les passionner. C’est en cela évidemment que le piège est en route. A travers le roman, je tends un piège au lecteur. Car tous les faits rapportés sont authentiques.

**Pourquoi l’ambassade de Belgique a-t-elle exfiltré Alain-Charles Faidherbe ?**
Je ne peux pas rentrer dans les détails mais il était en contact avec l’antenne belge d’Amnesty International. A la fin du livre, quand le tueur lui annonce qu’il a deux jours pour déguerpir, son dossier était prêt pour l’exfiltration. Son cas était bien documenté sur internet.

**Vous avez récemment posé à ses côtés pour un hebdomadaire à grande diffusion. Son portrait, à peine flou, a été publié. On peut le reconnaître. Que risque-t-il ?**
J’ai vu des photos de lui quand il était jeune et il a beaucoup changé. J’ignore les risques qu’il court mais à mon avis ils ne sont pas très élevés car les faits sont anciens. De plus, on ne précise pas le nom du pays d’où il vient.

**Dans quelle mesure le parcours d’Alain-Charles Faidherbe, dans ce qu’il a de plus violent, fait-il écho à votre propre parcours ?**
Il n’y a pas de commune mesure. Je n’ai jamais vécu ce qu’il a vécu.

**Avez-vous jamais fait l’objet de tentatives de corruption ?**
Oui, bien sûr. Un jour, des gens m’ont suivi à la sortie du Palais. Ils étaient sur écoute téléphonique, et donc je savais pertinemment ce qui allait m’arriver à chaque seconde. Je ne peux pas rentrer dans les détails, le procès n’est pas terminé. Mais ils allaient me proposer une prostituée. Je savais que ça allait se produire, mais je me suis levé et suis parti.

**Vous avez aussi été étrangement perquisitionné…**
J’ai fait l’objet, il y a quatre ans, d’une rumeur selon laquelle j’aurais été corrompu. J’ai été perquisitionné, mais les policiers n’ont rien saisi ! Le dossier n’étant pas bouclé et une autre personne étant visée, ça fait quatre ans qu’on me refuse l’accès au dossier. C’est ce qui m’est arrivé de plus terrible. Mais ça ne m’a pas empêché d’être renouvelé dans mon mandat, et de recevoir parmi les plus gros dossiers de Belgique. Je n’ai rien à me reprocher, je suis un livre ouvert. Mais le nombre de fois où mes collègues ont fait l’objet de plaintes contre eux et d’enquêtes…

**Et vous même ?**
Je suis une fois allé en perquisition au Pakistan, à Peshawar, en pays taliban à la frontière avec l’Afghanistan. Les personnes visées ont déposé plainte contre moi pour violation de domicile et j’ai été poursuivi en correctionnelle au Pakistan ! J’étais défendu par le bâtonnier d’Islamabad et j’ai été acquitté. Mais ça a quand même généré une certaine pression et coûté des honoraires à l’Etat belge. J’ai toujours le jugement. Je l’ai encadré !

**Des menaces ?**
Oui, j’ai aussi fait l’objet de menaces de mort ciblées. En tout, j’ai fait l’objet de deux contrats sur ma tête. Le premier venait du milieu des carrousels TVA, le second était lié au blanchiment de stupéfiants. J’ai été appelé au bureau du procureur général et placé sous protection policière. Des détectives privés m’ont suivi, aussi – des détectives étrangers, car les belges ne s’attaquent pas aux magistrats. C’est en perquisitionnant une société, cotée en bourse, que je suis tombé sur les rapports des détectives privés me concernant.

**Comment avez-vous découvert l’existence de contrats sur votre tête ?**
La sûreté de l’Etat m’en a averti.

**Ils sont intervenus avant que vous ne receviez des menaces formelles ?**
Absolument. Dans un autre dossier de blanchiment de stups, un policier a été payé pour donner l’adresse de mes enfants. Un policier zonal. Il a été exclu de la police. Ça, disons que c’est un peu plus embêtant... Mais il y a des contre-attaques qui se font du côté de la Sûreté de l’Etat, du côté policier, du côté du Parquet général. C’est assez efficace. Contrairement à certains pays, comme l’Italie ou la Bulgarie, je ne pense pas qu’un juge soit vraiment en danger aujourd’hui en Belgique.

**Comment gère-t-on tout ça sur le plan personnel ?**
Ça ne m’a jamais empêché de dormir. Je n’ai jamais éprouvé la moindre crainte pour ma vie. Mais quand on m’a mis en cause, il y a quatre ans, j’en ai été malade. J’avais un sentiment d’injustice qui remontait et j’ai dû combattre cela pendant un an ou deux.

**Des pressions internes pour « enterrer » certains dossiers, ça arrive ?**
Jamais. Je n’ai jamais reçu aucune directive. Je n’ai aucune pression. Je suis un juge totalement indépendant et je suis très respecté par mon président de tribunal.

**La justice belge est souvent lente. Combien d’affaires gérez-vous ?**
Des gros dossiers, type HSBC ou UBS, j’en ai 37 sur près de 300 dossiers ouverts dans mon cabinet. C’est beaucoup. Mais je n’ai pas de retard. Pas un jour. J’ai une équipe de policiers remarquables. Le problème se trouve plutôt au Parquet, qui manque d’effectifs. Et à la Cour d’Appel aussi, où il faut souvent des années avant que les arrêts soient rendus.

**Question de moyens ?**
En première instance, le président du tribunal a bien renforcé la chambre financière : il a doublé les effectifs. Maintenant, les procédures sont souvent stratégiques. Les avocats exploitent à fond les failles de la loi Franchimont [loi du 12 mars 1998 modernisant la procédure pénale, NDLR]. Par exemple, quand une instruction est terminée, qu’elle a été transmise au Parquet et que celui-ci a tracé ses réquisitions, c’est à ce moment-là qu’il faudrait que la défense puisse demander des devoirs d’enquête complémentaires. Pas en chambre du conseil ! Car une fois les réquisitions tracées, il faut encore attendre un à deux ans avant que le dossier ne passe en chambre du conseil. Et il ne se passe rien pendant ce temps-là. Rien ! Un an de perdu à ce moment-là, c’est trois ans de perdus en bout de course. C’est terrible. Et si je refuse des devoirs complémentaires demandés, certains avocats vont systématiquement en appel devant la chambre des mises en accusation. Et là, il faut deux années supplémentaires avant que ça ne soit traité. C’est très frustrant.

**Comment solutionner ce problème ?**
Il faut renforcer la section financière du Parquet et modifier la loi Franchimont. J’ai par exemple bouclé un dossier sensible pour lequel il a fallu attendre cinq ans pour que le Parquet trace ses réquisitions. Il demande la prescription…

**Ça doit vous rester en travers de la gorge, non ?**
Quand j’ai transmis mon dossier au Parquet, mon job est terminé. Si ça devait me rester en travers de la gorge, il y a longtemps que j’aurais changé de métier.

**Que pensez-vous de la loi élargissant la transaction pénale aux crimes financiers ?**
Je suis favorable au principe, mais le problème est qu’on exclut le juge d’instruction de la transaction. Le fait qu’il n’y ait pas de reconnaissance de culpabilité pour les personnes qui transigent – comme cela existe aux Etats-Unis ou en France – me gêne aussi. Elles n’ont pas de casier judiciaire et on ne retrace jamais l’origine des fonds. Ça ne va pas. Le lobby des diamantaires anversois a poussé cette loi au moment où les premières attaques sont apparues, afin de leur permettre de s’en sortir pénalement. Laurette Onkelinx a fait passer cette loi, qui est tellement mal rédigée qu’il a fallu la rectifier deux mois après son vote.
Propos recueillis par D.Lp

[Référence livre]
Les poches cousues, Michel Claise et Alain-Charles Faidherbe, Editions Luce Wilkin, 285 p., 21 €.


