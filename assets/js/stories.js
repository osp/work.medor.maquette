$(function() {
    $('#stories [data-src]').each(function() {
        var src = $(this).attr('data-src');
        $(this).load(src, function() {});
    })
});
