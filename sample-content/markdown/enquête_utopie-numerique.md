De l’utopie numéri-que au choc social
=============================

Quarante-cinq ans après les premiers pas de l’homme sur la Lune, la course technologique emprunte une voie singulière : en janvier dernier, un réfrigérateur connecté à Internet envoyait inopinément des rafales de courriels indésirables... Au-delà de son folklore, la numérisation de la vie quotidienne engendre un modèle économique qui contraste avec les promesses mirifiques de la Silicon Valley.
{: .chapeau}

<figure>
  <img src="sample-content/img/0026.jpg" alt="0026.jpg">
  <figcaption>Mrzyk & Moriceau, http://1000dessins.com</figcaption>
</figure> 

Dans la «&nbsp;salle de bains connectée&nbsp;», la brosse à dents interactive lancée cette année par la société Oral-B (filiale du groupe Procter & Gamble) tient assurément la vedette : elle interagit — sans fil — avec notre téléphone portable tandis que, sur l’écran, une application traque seconde par seconde la progression du brossage et indique les recoins de notre cavité buccale qui mériteraient davantage d’attention. Avons-nous brossé avec suffisamment de vigueur, passé le fil dentaire, gratté la langue, rincé le tout ?

Mais il y a mieux. Comme l’affiche fièrement le site qui lui est consacré [^1], cette brosse à dents connectée «&nbsp;convertit les activités de brossage en un ensemble de données que vous pouvez afficher sous forme de graphiques ou partager avec des professionnels du secteur&nbsp;». Ce qu’il adviendra par la suite de ces données fait encore débat : en conserverons-nous l’usage exclusif ? Seront-elles captées par des dentistes professionnels ou vendues à des compagnies d’assurances ? Rejoindront-elles le flux des informations déjà engrangées par Facebook et Google ?

La prise de conscience soudaine que les données personnelles enregistrées par le plus banal des appareils ménagers — de la brosse à dents aux toilettes « intelligentes » en passant par le réfrigérateur — pouvaient se transformer en or a soulevé une certaine réprobation vis-à-vis de la logique promue par les mastodontes de la Silicon Valley.

Ces entreprises collectent à grande échelle les traces laissées par les internautes sur les sites qu’ils fréquentent, les utilisent pour leur propre compte et les revendent aux annonceurs ou à d’autres sociétés. Elles engrangent ainsi des milliards de dollars, tandis que les utilisateurs — nous — accèdent simplement à quelques services gratuits. Face à ce constat émerge une critique bizarre, aux connotations populistes : contestons ces monopoles, clame-t-elle, et remplaçons-les par une multitude de petits entrepreneurs. Chacun de nous pourrait constituer son propre portefeuille de données et tirer bénéfice de leur commerce en vendant, par exemple, ses données de brossage à un fabricant de dentifrice, son génome à un laboratoire pharmaceutique, ou en révélant sa géo-localisation en échange d’une ristourne au restaurant du coin.

Des voix influentes, comme celles de l’essayiste et chef d’entreprise Jaron Lanier ou du chercheur en informatique Alex «&nbsp;Sandy&nbsp;» Pentland, célèbrent ce nouveau modèle [^2].

Ces voix nous promettent un monde où la protection de la vie privée serait assurée : si l’on considère les données comme une propriété privée, alors un solide arsenal juridique et des technologies adéquates pourraient garantir qu’aucun tiers ne les pille. Mais elles nous font aussi miroiter un avenir de prospérité. Par quel miracle ? Celui de l’« Internet des objets », c’est-à-dire la prolifération d’appareils grâce auxquels nos moindres faits et gestes seront recensés, analysés et... monétisés. 

<!-- 0006.jpg : image sur la deuxième colonne, à cheval sur le blanc de la marge en haut -->

Quelque part, quelqu’un est disposé à payer pour savoir ce que nous chantons sous la douche. S’il ne s’est pas encore manifesté, c’est simplement parce qu’aucun capteur sonore connecté à Internet n’équipe notre salle de bains.
{: .exergue}

<figure>
  <img src="sample-content/img/0006.jpg" alt="0006.jpg">
  <figcaption>Mrzyk & Moriceau, http://1000dessins.com</figcaption>
</figure>

Les enjeux sont clairs. Si Google truffe notre maison de jolis capteurs intelligents fabriqués par sa filiale Nest, c’est Google, et pas nous, qui gagnera de l’argent lorsque nous chantonnerons. La stratégie du géant consiste à agréger des données provenant d’une multitude de sources (voiture sans conducteur, lunettes connectées, courrier électronique) et à faire dépendre l’efficacité du système de son ubiquité : pour en tirer le meilleur parti, nous devrions laisser ses services emplir, tel un gaz, les moindres recoins de notre quotidien. L’immensité du réservoir de données ainsi constitué le protège de toute concurrence, et les entreprises de moindre envergure l’ont bien compris. Dès lors, il ne leur reste qu’une option : répondre à l’appel de Pentland et de Lanier, et contrecarrer Google en exigeant que les données appartiennent par défaut aux utilisateurs, ou que ces derniers touchent au moins une part des bénéfices.

<figure>
  <img src="sample-content/img/0320.jpg" alt="0320.jpg">
  <figcaption>Mrzyk & Moriceau, http://1000dessins.com</figcaption>
</figure>

Divergentes en apparence, ces deux stratégies s’abreuvent à la même source idéologique, dont elles représentent deux variantes intellectuelles. Comme l’explique le sociologue britannique William Davies [^3], la vision proposée par Pentland et Lanier se rattache à la tradition «&nbsp;ordolibérale&nbsp;» allemande, qui élève la concurrence au rang d’impératif moral et considère donc tout monopole comme un danger. Moins obsédée par la morale que par l’efficacité économique et l’intérêt du consommateur, l’approche de Google, quant à elle, rejoint l’idéologie néolibérale américaine incarnée par l’école de Chicago. Selon elle, les monopoles ne sont pas nuisibles par nature ; certains peuvent même jouer un rôle social positif. Malgré ses prétentions à l’innovation et au chamboulement de l’ordre établi, le débat contemporain sur la technologie reste donc engoncé dans un carcan familier : considérant l’information comme une marchandise, il s’intègre parfaitement au paradigme libéral.

Pour concevoir l’information autrement, il faudrait commencer par l’extraire de la sphère économique. Peut-être en la considérant comme un « commun », notion chère à une certaine gauche radicale. Mais il serait auparavant fort utile de se demander pourquoi l’on accepte comme une évidence la marchandisation de l’information. La réponse tient dans le rôle que la phase historique actuelle assigne à la technologie : celui de deus ex machina créateur d’emplois. Elle doit stimuler l’économie et combler les déficits budgétaires engendrés par l’évasion fiscale des riches et des multinationales. Dans un tel contexte, ne pas considérer l’information comme une marchandise reviendrait pour les dirigeants politiques à crever leur propre bouée de sauvetage.

Retour au XIX<sup>e</sup> siècle
--------------------------------

Même les observateurs les plus perspicaces de la crise financière sous-estiment le poids de cette croyance dans l’omnipotence de la technologie. Ainsi le sociologue allemand Wolfgang Streeck [^4] explique-t-il qu’au début des années 1970, lorsque apparurent les premiers signes de l’effondrement du modèle social issu du compromis d’après-guerre, les dirigeants occidentaux mirent en œuvre trois stratégies pour gagner du temps et maintenir le statu quo : l’inflation, l’endettement des Etats et, finalement, l’encouragement tacite à l’endettement des particuliers, auxquels le secteur privé vend des prêts immobiliers et des crédits à la consommation. Au nombre de ces dispositifs visant à retarder l’inévitable, Streeck ne mentionne pas les technologies de l’information.

Celles-ci créent à la fois de la richesse et des emplois — à condition que chacun se transforme en entrepreneur et apprenne à programmer pour écrire des applications. Parmi les premiers, le gouvernement britannique a concrétisé ce potentiel à l’échelle nationale en tentant de vendre les données de malades aux compagnies d’assurances (mais une vague de protestation populaire a mis un terme à cette initiative), ou les données personnelles d’étudiants aux opérateurs de téléphonie mobile et aux vendeurs de boissons énergisantes. Un récent rapport, financé en partie par Vodafone, affirme que l’on pourrait générer 16,5 milliards de livres (21 milliards d’euros) en aidant les consommateurs à gérer, c’est-à-dire à vendre, leurs données personnelles [^5]. Le rôle de l’Etat se limiterait à définir un cadre légal pour les intermédiaires qui effectueraient les transactions entre consommateurs et fournisseurs de services.

Tandis que les Etats s’efforcent de gagner du temps par le haut, les start-up de la Silicon Valley, elles, proposent des solutions pour gagner du temps par le bas. Elles placent ainsi une foi inébranlable dans des services comme Uber (des particuliers convertissent leur voiture en taxi) et Airbnb (et leur appartement en hôtel), censés transformer des biens analogiques ringards en source de profits numériques et branchés. Objectif : assurer un complément de revenus à leur propriétaire. Comme l’explique M. Brian Chesky, le président-directeur général d’Airbnb, « le chômage et les inégalités sont au plus haut, mais nous sommes assis sur une mine d’or (...). Nous avons appris à créer nos propres contenus, mais nous pouvons désormais tous créer notre propre emploi et, pourquoi pas, notre propre secteur d’activité [^6] ».

Fidèle à son habitude, la Silicon Valley débobine ici la rhétorique communautaire de la contre-culture pour présenter Uber ou Airbnb comme les piliers de la nouvelle « économie du partage », horizon utopique rêvé par les anarchistes autant que par les libertariens, où désormais les individus traiteraient directement les uns avec les autres en court-circuitant les intermédiaires [^7]. Plus prosaïquement, il s’agit de remplacer des intermédiaires analogiques, comme les sociétés de taxis, par des intermédiaires numériques, comme Uber, entreprise financée par les anarchistes notoires de Goldman Sachs.

Les secteurs de l’hôtellerie et des taxis étant universellement détestés, le débat public s’est rapidement résumé à l’image d’audacieux précurseurs bousculant des rentiers poussifs et dépourvus d’imagination.
{: .exergue}

Une présentation aussi biaisée masque un fait essentiel : ces courageux champions de l’«&nbsp;économie du partage&nbsp;» évoluent dans un univers mental caractéristique du XIXe siècle. Dans leur système, le travailleur, radicalement individualisé, ne bénéficie que d’une protection sociale symbolique ; il assume les risques qui pesaient auparavant sur les employeurs ; ses possibilités de négociation collective se réduisent à néant.

Les défenseurs de ce nouveau modèle justifient une telle précarité par des arguments dignes du théoricien libéral Friedrich Hayek. Les mécanismes auto-régulateurs (c’est le marché qui atteste la qualité du chauffeur ou de l’hôte) étant plus efficaces que les lois, autant se débarrasser des lois. «&nbsp;Lorsque nous aurons construit des systèmes véritablement autocorrecteurs, assure le célèbre investisseur en capital-risque Fred Wilson, nous n’aurons plus besoin de régulateurs [^8]. » Il suffit pour cela de saturer la société de boucles de rétroaction, c’est-à-dire d’évaluations qualitatives fournies en continu par les acteurs du marché : les avis et commentaires des utilisateurs.

La numérisation de la vie quotidienne combinée à l’avidité déchaînée par la financiarisation laisse présager la transformation de toute chose — notre génome comme notre chambre à coucher — en bien productif. Pionnière de la « génomique personnalisée&nbsp;», Mme Esther Dyson, actionnaire principale de la société 23andMe, compare sa société à un « distributeur automatique qui vous donne accès aux richesses enfouies dans vos gènes [^9] ». Voilà donc l’avenir que nous promet la Silicon Valley : un nombre suffisant de capteurs connectés à Internet changera nos vies en distributeurs géants de billets.

Tôt ou tard, on percevra les réfractaires au salut par l’«&nbsp;économie du partage&nbsp;» comme des saboteurs de l’économie, et la rétention de données comme un gaspillage injustifiable de ressources susceptibles de contribuer à la croissance. Ne pas «&nbsp;partager&nbsp;» sera aussi honteux que de refuser de travailler, d’économiser ou de rembourser ses dettes, la morale recouvrant là encore d’un vernis de légitimité une forme d’exploitation.

Il n’est guère surprenant que les catégories sociales écrasées par le fardeau de l’austérité commencent à convertir leur cuisine en restaurant, leur voiture en taxi et leurs données personnelles en actif financier. Que peuvent-elles faire d’autre ? Pour la Silicon Valley, nous assistons là au triomphe de l’esprit d’entreprise, grâce au développement spontané d’une technologie détachée de tout contexte historique, et notamment de la crise financière. En réalité, ce désir d’entreprendre est aussi joyeux que celui des désespérés du monde entier qui, pour payer leur loyer, en viennent à se prostituer ou à vendre des organes. Les Etats tentent parfois d’endiguer ces dérives, mais il leur faut équilibrer le budget. Alors, autant laisser Uber et Airbnb exploiter la « mine d’or » comme bon leur semble. Cette attitude conciliante présente le double avantage d’augmenter les rentrées fiscales et d’aider les citoyens ordinaires à boucler leurs fins de mois.

Une critique réduite à la lamentation
-------------------------------------

Toutefois, l’«&nbsp;économie du partage&nbsp;» ne supplantera pas celle de la dette : leur destin est au contraire de coexister. L’omniprésence des données couplée à l’efficacité grandissante des outils d’analyse permettront même aux banques de vendre du crédit à une clientèle considérée jusque-là comme insolvable — mais, cette fois, après écrémage numérique des mauvais éléments. Ainsi, des start-up comme Zest-Finance aident déjà les banques à filtrer les demandes de prêt en ligne en fonction de soixante-dix mille critères, parmi lesquels votre manière de taper sur un clavier ou d’utiliser votre téléphone. En Colombie, la jeune société de prêt Lenddo conditionne l’octroi de cartes de crédit au comportement des candidats sur les réseaux sociaux : chacun de leurs clics entre en ligne de compte. Une évidence qui n’échappe pas à M. Douglas Merrill, cofondateur de ZestFinance, dont la page d’accueil claironne : «&nbsp;Toute donnée personnelle est pertinente en termes de crédit.&nbsp;» Et si tel est le cas, alors notre vie elle-même, intégralement observée par les capteurs qui nous entourent, peut commencer à battre au rythme de la dette.

Les idiots utiles de la Silicon Valley rétorqueront qu’ils sont en train de sauver le monde. Si les pauvres demandent à s’endetter, pourquoi ne pas les aider ? Que ce besoin de crédit puisse découler de l’augmentation du chômage, de la réduction des dépenses sociales ou de l’effondrement des salaires réels n’effleure pas ces esprits visionnaires. Ni d’ailleurs l’idée que d’autres politiques économiques pourraient inverser ces tendances, et rendre inutiles ces merveilleux outils numériques permettant de vendre toujours plus de dette. Leur tâche unique — et leur unique source de revenus — consiste à créer des outils pour résoudre les problèmes tels qu’ils se présentent au jour le jour, et non à développer une analyse politique et économique susceptible de reformuler ces problèmes pour s’attaquer à leurs causes.

En cela, la Silicon Valley ressemble à toutes les autres industries : à moins qu’elles puissent en tirer profit, les entreprises ne veulent pas d’un changement radical de société. Toutefois, Google, Uber ou Airbnb disposent d’un répertoire rhétorique nettement plus vaste que celui de JP Morgan ou de Goldman Sachs. S’il vous prend l’envie de critiquer les banques, vous passerez pour un adversaire du capitalisme, un pourfendeur de Wall Street et de son sauvetage par les contribuables : une position désormais si banale qu’elle fait parfois bâiller. Critiquer la Silicon Valley, en revanche, revient à passer pour un technophobe, un benêt nostalgique du bon vieux temps d’avant l’iPhone. De même, toute critique politique et économique formulée à l’encontre du secteur des technologies de l’information et de ses liens avec l’idéologie néolibérale est instantanément galvaudée en critique culturelle de la modernité. Et son auteur, dépeint en ennemi du progrès, qui rêverait de rejoindre Martin Heidegger dans la Forêt-Noire pour contempler tristement le béton sans âme des barrages hydroélectriques.

A cet égard, les lamentations incessantes sur le déclin de la culture engendré par Twitter et les livres électroniques ont joué un rôle calamiteux. Au début du XX<sup>e</sup> siècle, le philosophe Walter Benjamin et le sociologue Siegfried Kracauer considéraient les problèmes posés par les nouveaux médias à travers un prisme socio-économique. Aujourd’hui, il faut se contenter des réflexions d’un Nicholas Carr, obsédé par les neurosciences, ou d’un Douglas Rushkoff, avec sa critique biophysiologique de l’accélération [^10]. Quelle que soit la pertinence de leur contribution, leur mode d’analyse finit par découpler la technologie de l’économie. On se retrouve alors à débattre de la manière dont un écran d’iPad conditionne les processus cognitifs de notre cerveau, au lieu de comprendre comment les données recueillies par notre iPhone influencent les mesures d’austérité de nos gouvernants.✶
Par Evgeny Morozov, août 2014.

Evgeny Morozov Auteur de To Save Everything, Click Here. Technology, Solutionism, and the Urge to Fix Problems That Don’t Exist, Allen Lane, Londres, 2013. A paraître en français le 21 août aux éditions FYP (Limoges) sous le titre Pour tout résoudre, cliquez ici. L’aberration du solutionnisme technologique.

[^1]: http://connectedtoothbrush.com


[^2]: Jaron Lanier, Who Owns the Future ?, Allen Lane, Londres, 2013, et Sandy Pentland, Social Physics. How Good Ideas Spread. The Lessons From a New Science, Penguin Press, New York, 2014.

[^3]: William Davies, The Limits of Neoliberalism. Authority, Sovereignty and the Logic of Competition, Sage, Londres, 2014.

[^4]: Wolfgang Streeck, Buying Time. The Delayed Crisis of Democratic Capitalism, Verso, Londres, 2014. Lire aussi, du même auteur, « La crise de 2008 a commencé il y a quarante ans », Le Monde diplomatique, janvier 2012.

[^5]: « Personal information management services : An analysis of an emerging market », 16 juin 2014.

[^6]: Cité par Rebecca Chao, « How the Internet saves at #PDF14 », 6 juin 2014.

[^7]: Lire Martin Denoun et Geoffroy Valadon, « Posséder ou partager ? », Le Monde diplomatique, octobre 2013.

[^8]: Cité par Ann Babe, « Writing the rules of the sharing economy », Techonomy.com, 6 juin 2014.

[^9]: « 23andMe... and me : Interview with Esther Dyson », 7 décembre 2009, http://blog.23andme.com Lire aussi Catherine Bourgain, « Génétique personnalisée sur Internet », Le Monde diplomatique, juin 2008.

[^10]: Nicholas Carr, Internet rend-il bête ?, Robert Laffont, Paris, 2011 ; Douglas Rushkoff, Present Shock : When Everything Happens Now, Penguin, New York, 2013.


