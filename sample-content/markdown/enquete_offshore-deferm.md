Enquête – Finance offshore

Drôle de manège entre Embourg, Gibraltar et les îles Vierges
=============================

Inculpé en octobre 2013 pour corruption dans le cadre du marché public de l’incinérateur d’Herstal, Léon-François Deferm cherche à investir, via des prête-noms, 10 millions d’euros dans un projet immobilier sur les hauteurs de Liège. Une partie des fonds provient d’une société écran de Gibraltar. La cellule antiblanchiment (CTIF) aurait par ailleurs été alertée de mouvements suspects sur le compte du fils de l’homme d’affaires…
{: .chapeau}

Inculpé il y a quelques mois par la justice liégeoise pour corruption active, faux et usage de faux, Léon-François Deferm, 70 ans, a reconnu devant les enquêteurs avoir touché un million d’euros de commissions occultes lors de l’attribution, à la société française Inova, du marché public relatif à la construction de l’incinérateur Uvelia de l’intercommunale Intradel à Herstal.
Pour rappel, grâce à des perquisitions et des commissions rogatoires internationales (notamment au Luxembourg), les enquêteurs de la police judiciaire de Liège détiennent les preuves que Léon-François Deferm a perçu, entre 2006 et 2008, un million d’euros d’Inova, via une cascade de fausses factures présumées navigant entre Paris, Spa, Vaduz et Luxembourg. Objectif : remercier l’homme d’affaires pour son « coup de pouce » ayant permis à Inova de remporter le lucratif marché liégeois de 170 millions d’euros.

Un million à Panama et Vaduz 
-----------

Du million palpé par Deferm, 583 000 euros se sont retrouvés sur le compte luxembourgeois de Cartwright Corp. Inc. (une société écran panaméenne) et 352 000 euros sur celui de Robiro-Invest (une offshore établie au Liechtenstein), deux opaques véhicules financiers contrôlés par Deferm.
L’homme d’affaires tenterait-il à présent de réinvestir tout ou partie de ces commissions occultes dans un projet immobilier à Embourg ? Ou en dépenses diverses en utilisant le compte bancaire de son fils pour brouiller les pistes ? Ces questions n’ont rien de saugrenu en regard de l’opacité extrême entourant deux récentes opérations financières dont Marianne a pu prendre connaissance.
De quoi parle-t-on ? Du rapatriement, en Belgique, de fonds détenus par deux obscures sociétés écrans basées à Gibraltar et aux îles Vierges britanniques. Vu la destination des fonds – des sociétés belges dont les enfants Deferm hériteront ou sont actionnaires – et le goût immodéré de Léon-François Deferm pour les montages offshore (lire encadré p. XX), il semble plus que probable que ces sociétés écrans soient contrôlées par l’homme d’affaires liégeois. Mais pourquoi diable tant d’opacité si l’origine des fonds est légale ?

Un boulet pour Bacquelaine
-----------

La première opération suspecte intervient dans le cadre d’un ambitieux projet immobilier dans la banlieue cossue de Liège : la construction de 32 appartements répartis sur quatre immeubles sur le site d’un ancien manège de 7 000 m² à Embourg. Estimé à 10 millions d’euros, le projet est porté par la société belge Alizé du Sud, qui a introduit en octobre 2013 une demande de permis d’urbanisme à la commune de Chaudfontaine (qui englobe Embourg).
Dix offres avaient été remises, en janvier 2012, à l’administration communale pour l’achat du terrain. Alizé l’avait emporté avec une offre de 1 201 000 euros, soit bien plus que l’estimation de 700 000 euros réalisée pour la vente. Daniel Bacquelaine, député-bourgmestre MR de Chaudfontaine et tête de liste à la Chambre pour la circonscription de Liège le 25 mai prochain, buvait du petit lait. Mais sait-il que les droits d’enregistrement du terrain versés au notaire proviennent d’une obscure offshore de Gibraltar, Congleton Properties Limited ?

Prête-noms à tous les étages
-----------

Minuscule territoire d’outre-mer du Royaume-Uni, Gibraltar, 29 000 habitants, est un satellite de la City de Londres. Un fournisseur d’opacité officiel de la Couronne d’Angleterre, à l’instar de Jersey ou des îles Vierges. Congleton, créée le 19 avril 2012 – soit six semaines avant l’expiration du délai légal pour payer le terrain – est entièrement pilotée par la société fiduciaire Gibro Corporate Management. Laquelle fournit des prête-noms pour les administrateurs, mais aussi pour les… actionnaires.
Ainsi, aux yeux des autorités locales, Congleton appartient à Gibro, officine gérée par un certain Stuart Rodriguez, avocat britannique de son état. Seul Rodriguez connaît donc le nom du ou des véritables actionnaires de Congleton. On fait difficilement plus opaque…

Prêt «offshore» de 200.000 euros 
-----------
Selon nos informations, depuis une augmentation de capital réalisée en décembre 2013, Alizé du Sud appartient à 79 % à SFMI SA, une société belge contrôlée par Léon-François Deferm via différents prête-noms. Deux de ses enfants posséderaient chacun 4,2 % d’Alizé et… Congleton les 12,6 % restants.
Le 20 avril 2013, Michel Vander Elst, l’homme-lige de Léon-François Deferm et représentant d’Alizé, envoie un mail au notaire Stéphane Delange. Ce dernier chapeaute l’opération immobilière liégeoise. Vander Elst l’informe de l’existence d’un « prêt à Alizé pour le paiement des droits de mutation du terrain d’Embourg ». Le montant ? 200 000 euros. Le prêteur ? Congleton. Le but du prêt ? Payer les droits d’enregistrement du terrain au notaire. Il était temps. Le terrain avait été payé in extremis à la commune le 30 mai 2012, dernier jour du délai légal. Le notaire aura pour sa part encore dû patienter près d’un an supplémentaire !

160.000 euros virés depuis Tortola 
-----------

La deuxième opération suspecte serait quant à elle remontée jusqu’à la Cellule de traitement des informations financières (CTIF), le dispositif fédéral de lutte contre le blanchiment et le financement du terrorisme. Selon nos informations, la CTIF aurait reçu fin 2012 une déclaration de soupçons de blanchiment de la banque Belfius, suite à une alerte lancée par son agence d’Embourg.
Cette déclaration – obligatoire pour une série de professionnels (banquiers, assureurs, notaires, avocats…) témoins de mouvements de fonds suspects supérieurs à 10 000 euros – est liée à un obscur virement étranger réalisé sur le compte du fils de Léon-François Deferm par une société offshore contrôlée semble-t-il par l’homme d’affaires liégeois.
Le mardi 27 novembre 2012, Canongate Group Limited, société écran des îles Vierges britanniques, vire 160 000 euros sur le compte BE68 0835 9568 9834 de V. Deferm ouvert à l’agence Belfius située Voie de l’Ardenne à Embourg. Il faut savoir que le jeune homme, âgé de 20 ans, souffre d’un handicap mental et n’est pas en mesure de gérer lui-même ses finances. 

Compte de transit
-----------

Son compte a semble-t-il été utilisé comme « compte de transit », vraisemblablement par son père, actionnaire présumé de Canongate. En effet, dans les trois jours qui suivirent l’arrivée des fonds, ceux-ci ont été redirigés vers d’autres comptes : 15 000 puis 8 000 euros ont été virés à la société belge Immo 30 contrôlée par Léon-François Deferm, 15 000 euros ont atterri sur le compte d’une de ses filles, 19 000 euros sur un compte destiné à payer les loyers en 2013 d’une maison occupée par son ex-épouse, et 103 000 euros sur un autre compte ouvert au nom de V. Deferm – et donc non piloté personnellement par le jeune homme.
D’où proviennent l’argent de Congleton et Canongate ? Mystère. Il pourrait certes s’agir de fonds gagnés légalement par l’homme d’affaires. Mais dans ce cas, pourquoi tant d’opacité ? Pourquoi se cacher derrière prête-noms et sociétés écrans dans des paradis fiscaux sulfureux ?
David Leloup


###Un habitué des véhicules offshore

Léon-François Deferm profite du charme discret des paradis fiscaux depuis au moins quatre décennies. Et ce grâce à son éminence grise, l’avocat déchu Michel Vander Elst, qui réalise tous les montages offshore. Canongate Group (îles Vierges) et Congleton (Gibraltar) ne sont que deux récents soubresauts d’une longue saga offshore dont Marianne vous livre quelques uns des épisodes précédents.
**Robiro-Invest** (Liechtenstein). A Vaduz, Léon-François Deferm se cache derrière Robiro-Invest, créée en 1973. Cet Anstalt (établissement), géré localement par le petit-fils d’un ancien patron du fisc liechtensteinois, a récupéré 352 000 euros du million de commission occulte touchée par Deferm dans l’affaire Intradel.
**Tradeast Network Limited **(Gibraltar). Créée le 15 septembre 1994 et dissoute en 2002, cette offshore aurait été créée pour racheter GPOM, le groupe de l’homme d’affaires congolais Pierre Otto Mbongo, proche du président du Congo-Brazzaville Denis Sassou-Nguesso. Fin 1994, le président de l’offshore est Léon-François Deferm et son vice-président Philippe Cravate.
**Sumo SA**(Luxembourg). Créée en 1989 et gérée par la fiduciaire de Robert Reckinger, l’ex-président de la Banque de Luxembourg, Sumo SA a compté parmi ses administrateurs Deferm, Vander Elst, Lautraite et Scailquin (voir encadré p. XX). Elle a été liquidée en juin 2010, juste après les perquisitions chez Vander Elst à Bruxelles dans le cadre de l’enquête Intradel.
**Cartwright Corp. Inc.**(Panama). Les Reckinger père et fils avaient déjà créé, en 1984 à Panama, l’offshore Cartwright Corp. Inc. pour Deferm. Celle-ci a récupéré, sur son compte luxembourgeois, 583 000 euros du million de commission touché par l’homme d’affaires dans le dossier Intradel. En août 2008, Cartwright a payé la location du yacht White Lady of Man pour Alain Mathot sur la Riviera. L’offshore a été dissoute en août 2013, quelques semaines après les révélations de Marianne sur son rôle dans l’affaire Intradel. D.Lp



###« Il voulait faire remonter les bénéfices aux Antilles néerlandaises »
*« Quand on a décidé de développer Comgis, Deferm a immédiatement proposé une structure où les bénéfices remontaient dans une société aux Antilles néerlandaises. Cela semblait pour lui un montage évident »*, se souvient Laurent Minguet. Cet entrepreneur liégeois et son associé Claude Darimont ont un temps côtoyé Léon-François Deferm comme actionnaires au sein de Comgis, une société de cartographie numérique. Mais Deferm a tenté de doubler ses deux partenaires lors de tractations avec des investisseurs vietnamiens. En *« mésintelligence profonde »* selon la justice, Deferm a essayé d’obtenir des droits de distribution concurrents sous le nom de sa société luxembourgeoise Sumo SA. En 2005, la Cour d’appel de Liège a confirmé la condamnation de Deferm en première instance, et l’a contraint à rendre à Minguet et son associé, pour un euro symbolique, les 440 actions qu’il détenait dans le capital de Comgis. D.Lp



###Les « seconds rôles » d’un passé trouble

Les prête-noms belges de Léon-François Deferm apparaissent dans les grandes affaires qui ont secoué la Belgique ces 25 dernières années.
{: .chapeau}

Agusta, Dutroux-Nihoul, VDB, Intradel… Autant d’affaires funestes qui ont ébranlé la Belgique ces 25 dernières années. Et dont on retrouve curieusement plusieurs acteurs secondaires parmi les administrateurs passés ou actuels d’Alizé du Sud et de SFMI, son actionnaire principal depuis décembre 2013.
**Michel Vander Elst.** Cet ancien avocat a été condamné à 8 ans de prison pour complicité dans l’enlèvement de VDB, l’ancien Premier ministre Paul Vanden Boeynants, par la bande de Patrick Haemers en janvier 1989. Dans la foulée, il noue une amitié avec Michel Nihoul, rencontré à la prison de Forest. Jugé, radié du barreau, il devient le bras droit de Léon-François Deferm et expert ès montages offshore. Vander Elst a été administrateur d’Alizé et de SFMI de 2005 à février 2013.
**Philippe Cravate.** Actionnaire de la société Management Car et associé de Vander Elst au sein de l’ASBL Egam dans les années 1990, on le retrouve avec Nihoul et Vander Elst au fameux barbecue du 9 août 1996, le soir de l’enlèvement de Laetitia Delhez par Marc Dutroux à Bertrix. Ce barbecue servira d’alibi à Nihoul. Domicilié à la résidence Marina de Pierre Otto Mbongo à Brazzaville au milieu des années 1990, Cravate fut administrateur d’Alizé et SFMI jusque fin 1997. Il vit désormais à Casablanca.
**Claude Scailquin.** La société Management Car, dirigée par Léon-François Deferm, Philippe Cravate et Claude Scailquin, a fourni entre 1991 et 1996 une Audi 80 en leasing à Nihoul, permettant à ce dernier de circuler incognito. Management Car était une filiale de Intres Management, qui a été rebaptisée… Alizé du Sud en février 2003. Scailquin démissionnera de son poste d’administrateur de SFMI en mai, puis d’Alizé en novembre.
**Gérard Martin.** En janvier 2004, ce Belge résidant à Séville (Espagne) accompagnait Deferm et Vander Elst lors de leur visite à la CNIM, société concurrente d’Inova pour le marché de l’incinérateur d’Herstal. Les trois hommes ont proposé à un cadre la garantie d’emporter le marché contre une commission de 8 %. Martin a été administrateur d’Alizé et de SFMI de 2000 à 2008.
**Jean-Philippe Lautraite.** En 1988, Deferm crée avec Lautraite et un Américain le groupe Trident pour reprendre, à Liège, la société informatique Unisys en faillite. Trident est suspectée d’avoir détourné une partie – on parle de 4,2 millions d’euros – des commissions versées à des intermédiaires par la firme italienne Agusta lors de l’achat par l’Etat belge, en 1988, de 46 hélicoptères de combat. En février 2013, Lautraite remplace Vander Elst comme administrateur d’Alizé et de SFMI. D.Lp


<!-- LEGENDES PHOTOS -->
Les 200 000 euros de droits d’enregistrement du terrain de l’ancien manège d’Embourg ont été payés au notaire via une société écran de Gibraltar.
{: .legende}

L’actionnaire officiel de Congleton Properties Limited est la fiduciaire Gibro sise au fond d’un discret piétonnier de Gibraltar…
{: .legende}

Canongate Group qui a attiré les soupçons de la cellule fédérale antiblanchiment (CTIF) a été créée le 13 mai 2010 aux îles Vierges britanniques, un des paradis fiscaux les plus opaques de la planète.
{: .legende}

Le projet immobilier de Léon-François Deferm pourrait bien empoisonner la campagne de Daniel Bacquelaine, député-bourgmestre MR de Chaudfontaine et tête de liste à la Chambre, le 25 mai à Liège.
{: .legende}

Michel Vander Elst, témoin lors du procès Dutroux en 2004, est l’éminence grise de Léon-François Deferm et l’architecte de ses montages offshore.
{: .legende}

