SHELL := /usr/bin/env bash

MD_FILES = $(shell find sample-content/markdown -type f -name '*.md')
HTML_FILES = $(patsubst sample-content/markdown/%.md, sample-content/html/%.html, $(MD_FILES))

all: $(HTML_FILES)

sample-content/html/%.html : sample-content/markdown/%.md
	mkdir -p $(@D)
	python bin/md2html.py $< | \
	python bin/microtypo.py - $@

.PHONY: clean
clean:
	rm -fr sample-content/html
